package de.awacedemy.blogTeam3.categories;

import de.awacedemy.blogTeam3.entry.Entry;
import de.awacedemy.blogTeam3.entry.EntryRepository;
import de.awacedemy.blogTeam3.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class CategoryController {

    private CategoryRepository categoryRepository;
    private EntryRepository entryRepository;

    @Autowired
    public CategoryController(CategoryRepository categoryRepository, EntryRepository entryRepository) {
        this.categoryRepository = categoryRepository;
        this.entryRepository = entryRepository;
    }

    @GetMapping("/makeCategories")
    public String makeCategories(Model model){
        model.addAttribute("category",new CategoryDTO(0,""));
        model.addAttribute("categories",categoryRepository.findAll());
        return "/makecategory";
    }

    @PostMapping("/addCategory")
    public String addCategory(@Valid @ModelAttribute CategoryDTO categoryDTO){
        Category category = new Category(categoryDTO.getCategoryName());
        for (Category category1 : categoryRepository.findAll()) {
            if(category.getName().equals(category1.getName())) {
                return "redirect:/makeCategories";
            }
        }
        categoryRepository.save(category);
        return "redirect:/";
    }

    @GetMapping("/{categoryname}")
    public String showCategoryPage(Model model, @PathVariable String categoryname){
        Optional<Category> categoryOptional = categoryRepository.findByName(categoryname);
        if(categoryOptional.isPresent()){
            Category category = categoryOptional.get();
            model.addAttribute(category);
            return "categorypages";
        }
        return "redirect:/";
    }

    @PostMapping("/addEntryCategory/{entryId}")
    public String addEntryCategory(@PathVariable Integer entryId,@Valid @ModelAttribute("categoryDTO")
            CategoryDTO categoryDTO, @ModelAttribute("sessionUser") User sessionUser) {
        Optional<Entry> optionalEntry = entryRepository.findById(entryId);
        if(optionalEntry.isPresent()){
            Entry entry = optionalEntry.get();
            if(sessionUser.isAdmin()) {
                Optional<Category> optionalCategory = categoryRepository.findById(categoryDTO.getCategoryId());
                if(optionalCategory.isPresent()) {
                    Category category = optionalCategory.get();
                    for (Entry entriesin : category.getEntries()) {
                        if (entriesin.getId().equals(entryId)) {
                            return "redirect:/";
                        }
                    }
                    entry.getCategories().add(category);
                    category.getEntries().add(entry);
                    entryRepository.save(entry);
                    categoryRepository.save(category);
                    return "redirect:/entryEdit/{entryId}";
                }
            }
        }
        return "redirect:/";
    }

}
