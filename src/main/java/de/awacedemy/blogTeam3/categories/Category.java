package de.awacedemy.blogTeam3.categories;

import de.awacedemy.blogTeam3.entry.Entry;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Category{
    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @ManyToMany
    List<Entry> entries;

    public Category() {
    }

    public Category(String name) {
        this.name = name;
        this.entries = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }
}
