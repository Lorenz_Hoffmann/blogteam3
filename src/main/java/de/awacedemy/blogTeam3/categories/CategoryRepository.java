package de.awacedemy.blogTeam3.categories;

import de.awacedemy.blogTeam3.comments.Comment;
import de.awacedemy.blogTeam3.entry.Entry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Integer> {
Optional<Category> findByName(String name);
}
