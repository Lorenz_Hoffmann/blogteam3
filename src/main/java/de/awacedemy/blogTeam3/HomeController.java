package de.awacedemy.blogTeam3;


import de.awacedemy.blogTeam3.categories.CategoryRepository;
import de.awacedemy.blogTeam3.entry.EntryRepository;
import de.awacedemy.blogTeam3.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {

    private EntryRepository entryRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public HomeController(EntryRepository entryRepository, CategoryRepository categoryRepository) {
        this.entryRepository = entryRepository;
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/")
    public String home(@ModelAttribute("sessionUser") User sessionUser, Model model) {
        model.addAttribute("entries", entryRepository.findAllByOrderByPostedAtDesc());
        model.addAttribute("categories", categoryRepository.findAll());
        return "home";
    }

}
