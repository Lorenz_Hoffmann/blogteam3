package de.awacedemy.blogTeam3.comments;

import javax.persistence.Column;
import javax.validation.constraints.Size;

public class CommentDTO {

    @Column(columnDefinition="TEXT")
    @Size(min = 1, max = 500)
    private String text;

    private Integer entryId;

    private Integer commentId;

    public CommentDTO(String text, Integer entryId,Integer commentId) {
        this.text = text;
        this.entryId = entryId;
        this.commentId = commentId;
    }


    //Getter
    public String getText() {
        return text;
    }

    public Integer getEntryId() {
        return entryId;
    }

    public Integer getCommentId() {
        return commentId;
    }

    //Setter

    public void setText(String text) {
        this.text = text;
    }

    public void setEntryId(Integer entryId) {
        this.entryId = entryId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

}
