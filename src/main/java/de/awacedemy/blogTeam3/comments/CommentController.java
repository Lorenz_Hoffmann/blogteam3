package de.awacedemy.blogTeam3.comments;

import de.awacedemy.blogTeam3.entry.Entry;
import de.awacedemy.blogTeam3.entry.EntryRepository;
import de.awacedemy.blogTeam3.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.Instant;
import java.util.Optional;

@Controller
public class CommentController {

    private CommentRepository commentRepository;

    private EntryRepository entryRepository;

    @Autowired
    public CommentController(EntryRepository entryRepository, CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
        this.entryRepository = entryRepository;
    }


    @PostMapping("/addComment/{entryId}")
    public String addComment(@Valid @ModelAttribute("newcomment") CommentDTO commentDTO, BindingResult bindingResult,
                             @ModelAttribute("sessionUser") User sessionUser){
        if (bindingResult.hasErrors()) {
            return "redirect:/entryEdit/{entryId}";
        }
        Optional<Entry> optionalEntry = entryRepository.findById(commentDTO.getEntryId());
        if(optionalEntry.isPresent()){
            Entry entry = optionalEntry.get();
            Comment comment = new Comment(commentDTO.getText(), Instant.now(), entry, sessionUser);
            commentRepository.save(comment);
        }
        return "redirect:/entryEdit/{entryId}";
    }

    @PostMapping("/removeComment/{entryId}")
    public String removeComment(@Valid @ModelAttribute("newcomment") CommentDTO commentDTO,  @ModelAttribute("sessionUser") User sessionUser){

        Optional<Comment> optionalComment = commentRepository.findById(commentDTO.getCommentId());
        if(optionalComment.isPresent()){
            Comment comment = optionalComment.get();
            commentRepository.delete(comment);
        }
        return "redirect:/entryEdit/{entryId}";
    }



}
