package de.awacedemy.blogTeam3.comments;

import de.awacedemy.blogTeam3.entry.Entry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {
    List<Comment> findAllByOrderByPostedAtAsc();
}
