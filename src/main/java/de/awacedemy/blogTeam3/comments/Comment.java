package de.awacedemy.blogTeam3.comments;


import de.awacedemy.blogTeam3.entry.Entry;
import de.awacedemy.blogTeam3.user.User;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.Instant;

@Entity
public class Comment {
    @Id
    @GeneratedValue
    private Integer id;


    @Column(length = 500, columnDefinition="TEXT")
    @Size(min = 1, max = 500)
    private String text;

    private Instant postedAt;

    @ManyToOne
    private Entry entry;

    @ManyToOne
    private User user;


    //Constructor

    public Comment() {
    }
    public Comment(String text,Instant postedAt, Entry entry, User user) {

        this.text = text;
        this.postedAt = postedAt;
        this.entry = entry;
        this.user = user;
    }


    //Getter

    public Integer getId() {
        return id;
    }

    public String getText() {
        return text;
    }


    public Instant getPostedAt() {
        return postedAt;
    }

    public Entry getEntry() {
        return entry;
    }

    public User getUser() {
        return user;
    }

    //Setter

    public void setId(Integer id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
