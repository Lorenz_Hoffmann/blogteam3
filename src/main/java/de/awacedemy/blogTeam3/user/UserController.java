package de.awacedemy.blogTeam3.user;
import de.awacedemy.blogTeam3.entry.Entry;
import de.awacedemy.blogTeam3.entry.EntryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.Instant;
import java.util.Optional;

@Controller
public class UserController {

    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("registration", new RegistryDTO("", "", ""));
        return "registration";
    }


    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("registration") RegistryDTO registration, BindingResult bindingResult) {

        if(!registration.getPassword1().equals(registration.getPassword2())) {
            bindingResult.addError(new FieldError("registration", "password2", "Passwords do not match."));
        }

        if(userRepository.existsByUsername(registration.getUsername())) {
            bindingResult.addError(new FieldError("registration", "username", "Username is already taken."));
        }

        if(bindingResult.hasErrors()) {
            return "registration";
    }

        User user = new User(registration.getUsername(), registration.getPassword1());
        userRepository.save(user);

        return "redirect:/login";
    }


    @GetMapping("/userlist")
    public String allusers(Model model) {
        model.addAttribute("userlist", userRepository.findAll());
        model.addAttribute("usertest", new UserDTO("", 0));
        return "userlist";
    }

    @PostMapping("/changeAdminStatus")
    public String changeAdminStatus(@Valid @ModelAttribute("usertest") UserDTO userDTO,
                                    @ModelAttribute("sessionUser") User sessionUser){
        Optional<User> optionalUser = userRepository.findById(userDTO.getId());
        if(optionalUser.isPresent()) {
            User user = optionalUser.get();
            if (sessionUser.isAdmin()) {
                if (user.isAdmin()) {
                    user.setAdmin(false);
                    userRepository.save(user);
                } else {
                    user.setAdmin(true);
                    userRepository.save(user);
                }
            }
        }


        return "redirect:/userlist";
    }
}
