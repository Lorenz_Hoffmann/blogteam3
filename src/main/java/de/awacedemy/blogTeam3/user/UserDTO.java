package de.awacedemy.blogTeam3.user;

public class UserDTO {

    private String username;
    private Integer id;

    public UserDTO(String username, Integer id) {
        this.username = username;
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
