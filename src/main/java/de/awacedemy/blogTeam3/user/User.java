package de.awacedemy.blogTeam3.user;

import de.awacedemy.blogTeam3.comments.Comment;
import de.awacedemy.blogTeam3.entry.Entry;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue
    private Integer id;

    private String username;
    private String password;
    private boolean isAdmin;

    //Lists

    @OneToMany(mappedBy = "user")
    private List<Entry> entryList;

    @OneToMany(mappedBy = "user")
    private List<Comment> commentList;

    //Constructor

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    //Getter

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public Integer getId() {
        return id;
    }

    //Setter

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
