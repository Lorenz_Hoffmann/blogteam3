package de.awacedemy.blogTeam3.entry;

import de.awacedemy.blogTeam3.categories.Category;
import de.awacedemy.blogTeam3.comments.Comment;
import de.awacedemy.blogTeam3.user.User;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;


@Entity
public class Entry {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(length = 500, columnDefinition="TEXT")
    @Size(min = 1, max = 500)
    private String title;

    @Column(length = 16000000, columnDefinition="TEXT")
    @Size(min = 1, max = 16000000)
    private String text;

    private Instant postedAt;

    @OneToMany(mappedBy = "entry")
    private List<Comment> comments;

    @ManyToOne
    private User user;

    @ManyToMany
    private List<Category> categories;


    //Constructor

    public Entry() {
    }

    public Entry(String title, String text, Instant postedAt, User user) {
        this.title = title;
        this.text = text;
        this.postedAt = postedAt;
        this.comments = new LinkedList<>();
        this.categories = new LinkedList<>();
        this.user = user;
    }

    //Getter


    public Integer getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public User getUser() {
        return user;
    }

    public List<Category> getCategories() {
        return categories;
    }

    //Setter


    public void setId(Integer id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
