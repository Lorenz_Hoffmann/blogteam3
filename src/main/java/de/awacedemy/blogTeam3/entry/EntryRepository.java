package de.awacedemy.blogTeam3.entry;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EntryRepository extends CrudRepository<Entry, Integer> {
    List<Entry> findAllByOrderByPostedAtDesc();
}
