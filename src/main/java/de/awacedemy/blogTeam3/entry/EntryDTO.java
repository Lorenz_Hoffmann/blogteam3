package de.awacedemy.blogTeam3.entry;

import javax.persistence.Column;
import javax.validation.constraints.Size;
import java.time.Instant;

public class EntryDTO {
    @Column(length = 500)
    @Size(min = 1, max = 500)
    private String title;

    @Column(length = 16000000)
    @Size(min = 1, max = 16000000)
    private String text;

    public EntryDTO(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public String getTitle() {
        return title;
    }
    public String getText() {
        return text;
    }
}
