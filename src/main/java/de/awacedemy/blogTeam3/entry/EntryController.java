package de.awacedemy.blogTeam3.entry;

import de.awacedemy.blogTeam3.categories.Category;
import de.awacedemy.blogTeam3.categories.CategoryDTO;
import de.awacedemy.blogTeam3.categories.CategoryRepository;
import de.awacedemy.blogTeam3.comments.Comment;
import de.awacedemy.blogTeam3.comments.CommentDTO;
import de.awacedemy.blogTeam3.comments.CommentRepository;
import de.awacedemy.blogTeam3.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Controller
public class EntryController {

    private EntryRepository entryRepository;
    private CommentRepository commentRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public EntryController(EntryRepository entryRepository, CommentRepository commentRepository,CategoryRepository categoryRepository) {
        this.entryRepository = entryRepository;
        this.commentRepository = commentRepository;
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/home")
    public String redirectHome(){
        return("redirect:/");
    }

    @GetMapping("/entry")
    public String entryPage(Model model){
        model.addAttribute("entry", new EntryDTO("", ""));
        return("entry");
    }

    @PostMapping("/addEntry")
    public String addEntry(@Valid @ModelAttribute("entry") EntryDTO entryDTO, BindingResult bindingResult,
                           @ModelAttribute("sessionUser") User sessionUser){
        if (bindingResult.hasErrors()) {
            return "entry";
        }

        Entry entry = new Entry(entryDTO.getTitle(), entryDTO.getText(), Instant.now(), sessionUser);
        entryRepository.save(entry);

        return "redirect:/";
    }

    @GetMapping("/EntrySuccess")
    public String entryEditSuccess() {
        return "entrySuccess";
    }

    @GetMapping("/entryEdit/{entryId}")
    public String entryEditPage(Model model, @ModelAttribute("sessionUser") User sessionUser,
                                @PathVariable Integer entryId) {
        Optional<Entry> optionalEntry = entryRepository.findById(entryId);
        if(optionalEntry.isPresent()) {
            Entry entry = optionalEntry.get();
            model.addAttribute("entry", entry);
            model.addAttribute("newcomment",new CommentDTO("",0,0));
            model.addAttribute("categories",categoryRepository.findAll());
            model.addAttribute("categoryDTO",new CategoryDTO(0,""));
            return "entryEdit";
        }
        return "redirect:/";
    }

    @PostMapping("/entryEdit/{entryId}")
    public String entryEdit(@Valid @ModelAttribute("entry") Entry entryTransfered, @PathVariable Integer entryId,
                            @ModelAttribute("sessionUser") User sessionUser, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "redirect:/entryEdit/{entryId}";
        }

        Optional<Entry> optionalEntry = entryRepository.findById(entryId);
        if(optionalEntry.isPresent()){
            Entry entry = optionalEntry.get();
            if(sessionUser.isAdmin()) {//change if admin or user who made the edit
                entry.setTitle(entryTransfered.getTitle());
                entry.setText(entryTransfered.getText());
                entryRepository.save(entry);
                return "entrySuccess";
            }
        }
        return "redirect:/";
    }

    @PostMapping("/entryDelete/{entryId}")
    public String entryDelete(@PathVariable Integer entryId, @ModelAttribute("sessionUser") User sessionUser) {
        Optional<Entry> optionalEntry = entryRepository.findById(entryId);
        if(optionalEntry.isPresent()) {
            Entry entry = optionalEntry.get();
            if (sessionUser.isAdmin()) {
                List<Comment> commentList = entry.getComments();

                commentRepository.deleteAll(commentList);

                for (Category category : entry.getCategories()){
                    category.getEntries().remove(entry);
                    categoryRepository.save(category);

                }

                entryRepository.delete(entry);
                return "redirect:/";

            }
        }
        return "redirect:/";
    }
}
