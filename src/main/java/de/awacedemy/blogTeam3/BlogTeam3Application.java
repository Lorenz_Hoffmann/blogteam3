package de.awacedemy.blogTeam3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogTeam3Application {

	public static void main(String[] args) {
		SpringApplication.run(BlogTeam3Application.class, args);
	}

}
